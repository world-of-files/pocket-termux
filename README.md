   Pocket Termux
=======================================

### About

  [Termux](https://termux.com/) is an Android terminal emulator and Linux application working like lightweight terminal Linux operating system.
  User may install additional packages by [package managers](https://wiki.termux.com/wiki/Package_Management) (pkg, apt) like in a real server.

  This git repository includes a script that initializes the Termux local environment,
  installs and configures base packages and system files to make Termux application usage comfortable.

### Installation

1. Install application from on your Android device from an application store
   - [Termux in Google Play](https://play.google.com/store/apps/details?id=com.termux) 
   - [Termux in F-Droid](https://f-droid.org/repository/browse/?fdid=com.termux)
2. Initialize your local server in application
3. Execute command in Termux terminal emulator:

    `pkg install wget && wget -c https://gitlab.com/world-of-files/pocket-termux/-/raw/master/init-termux.sh -O init-termux.sh && chmod +x init-termux.sh && ./init-termux.sh`
