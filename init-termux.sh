#$PREFIX/bin/bash

info() { echo -e "\e[32mINFO $@\e[0m" ;}

info "upgrade packages"
pkg upgrade -y || exit 1

info "install required packages"
pkg install -y \
        bzip2\
        cronie\
        curl\
        figlet\
        git\
        gzip\
        htop\
        lynx\
        man\
        ncdu\
        openssh\
        python\
        ranger\
        tar\
        taskwarrior\
        tmux\
        tree\
        unzip\
        vim\
        wget\
        || exit 1

info "installed packages list"
pkg list-installed

info "clone dotfiles repository"
rm -rf $HOME/git/world-of-files/dotfiles

git clone https://gitlab.com/world-of-files/dotfiles.git\
          $HOME/git/world-of-files/dotfiles/

mkdir -p $HOME/.vim

for file in tmux.conf vimrc taskrc ;do
    cp ${HOME}{/git/world-of-files/dotfiles/files/,/.}$file
done

info "update tmux.conf"
SED1='s| status-left .*| status-left "#[fg=cyan]:#S#[fg=green]:#I#[fg=yellow]:#P"|'
SED2='s| status-right .*| status-right ""|'
sed -i "$SED1;$SED2" $HOME/.tmux.conf

info "update taskrc"
SED='s|^include /usr/|### include /usr/|'
sed -i "$SED" $HOME/.taskrc

info "update bashrc"

cat > $HOME/.bashrc << EOF
#!\$PREFIX/bin/bash

### prevent the CPU from sleeping (optionally)
termux-wake-lock

### terminal decorations
PS1='\\e[\$([ \$? -gt 0 ] && echo 31 || echo 32);1m$\\e[0m '

### bash aliases
alias l='ls -1'
alias ll='ls -l'
alias la='ls -la'
alias dudu='du -sh *'
alias tree='tree -C'
alias findf='find . -type f -name'
alias findd='find . -type d -name'

### add ~/bin to PATH
export PATH=\$HOME/bin:\$PATH

### read local configuration files
for source_file in ~/.bash_{aliases,env} ;do
    [ -f \$source_file ] && source \$source_file
done

### start tmux automatically
[ -z \"\$TMUX\" ] && tmux
EOF

source $HOME/.bashrc

info "done.\n"

figlet "Success"
